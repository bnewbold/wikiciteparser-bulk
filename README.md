
wikiciteparser-bulk
===================

Quick script to extract references from Wikipedia bulk XML snapshots, outputing
JSON lines format.

Uses [python-mwxml](https://github.com/mediawiki-utilities/python-mwxml) to
iterate over the XML dump, and
[wikiciteparser](https://github.com/dissemin/wikiciteparser) to extract the
references.

## Quickstart

The assumption is that you would use this with one of the "current versions
only" dumps, like:

    https://dumps.wikimedia.org/enwiki/20210801/enwiki-20210801-pages-meta-current.xml.bz2

TODO:

1. install this script and dependencies on a linux machine
2. download an XML snapshot (big!)
3. run this script

## Prior Work

Similar projects include:

* [Harshdeep1996/cite-classifications-wiki](https://github.com/Harshdeep1996/cite-classifications-wiki):
  uses `wikiciteparser` and PySpark to extract references from bulk XML,
  outputs parquet. Requires a Spark cluster/environment to run. (itself used by
  [Wikipedia Citations in Wikidata](https://github.com/opencitations/wcw))
* [python-mwcites](https://github.com/mediawiki-utilities/python-mwcites): uses
  `python-mwxml` to iterate over bulk XML, has relatively simple identifier
  extraction
* [gesiscss/wikipedia_references](https://github.com/gesiscss/wikipedia_references):
  oriented towards tracking edits to individual references over time
